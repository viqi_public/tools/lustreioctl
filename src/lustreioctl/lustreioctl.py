import sys
import os
import ctypes
import fcntl
#import array
from ioctl_opt import IOR, IOW#, IOC, IOC_READ, IOC_WRITE
#from enum import Enum

class hsm_action():
    HUA_NONE = 1
    HUA_ARCHIVE = 10
    HUA_RESTORE = 11
    HUA_RELEASE = 12
    HUA_REMOVE  = 13
    HUA_CANCEL  = 14

class hsm_state():
    HS_NONE     = 0x00000000
    HS_EXISTS   = 0x00000001
    HS_DIRTY    = 0x00000002
    HS_RELEASED = 0x00000004
    HS_ARCHIVED = 0x00000008
    HS_NORELEASE= 0x00000010
    HS_NOARCHIVE= 0x00000020
    HS_LOST     = 0x00000040

HSM_STATES = [ (state, getattr(hsm_state, state)) for state in dir (hsm_state) if state.startswith ('HS') ]

def hsm_decode_state (hsm_state):
    labels = []
    for label,x in HSM_STATES[1:]:
        if x & hus_state:
            labels.append (label[2:].lower())
    return labels
    # rs = []
    # if state.hus_states & hsm_state.HS_RELEASED:
    #     rs.append('released')
    # if state.hus_states & hsm_state.HS_ARCHIVED:
    #     rs.append('archived')
    # if state.hus_states & hsm_state.HS_DIRTY:
    #     rs.append('dirty')
    # return rs



class lu_fid (ctypes.Structure) :
       _fields_ = [
           ('f_seq', ctypes.c_uint64),
           ('f_oid', ctypes.c_uint32),
           ('f_ver', ctypes.c_uint32),
       ]
lu_fid_ptr = ctypes.POINTER(lu_fid)

class hsm_extent (ctypes.Structure) :
       _fields_ = [
           ('offset', ctypes.c_uint64),
           ('length', ctypes.c_uint64),
       ]

class hsm_user_item(ctypes.Structure):
    _fields_ = [
        ('hui_fid', lu_fid),
        ('hui_extent', hsm_extent),
    ]

class hsm_request(ctypes.Structure):
    """https://github.com/tweag/lustre/blob/75f01f8bdff757a89c80aa4f0cc61608b9ff56e5/lustre/include/lustre/lustre_user.h
    Lines: 1162 - 1168
    """
    _fields_ = [
        ('hr_action', ctypes.c_uint32),
        ('hr_archive_id', ctypes.c_uint32),
        ('hr_flags', ctypes.c_uint64),
        ('hr_itemcount', ctypes.c_uint32),
        ('hr_data_len', ctypes.c_uint32),
    ]
hsm_request_ptr = ctypes.POINTER(hsm_request)



def create_hsm_user_request(paths,action):
    class hsm_user_request(ctypes.Structure):
        _fields_ = [
            ('hur_request', hsm_request),
            ('hur_user_item', hsm_user_item*len(paths)),
        ]
    hsm_user_request_ptr = ctypes.POINTER(hsm_user_request)
    request = hsm_user_request()
    request.hur_request.hr_action = action
    request.hur_request.hr_archive_id = 0
    request.hur_request.hr_flags = 0
    request.hur_request.hr_data_len = 0
    request.hur_request.hr_item_count = len(paths)
    #request.hur_request.hr_action = action
    for i, path in enumerate(paths):
        request.hur_user_item[i].hui_id = llapi_path2fid(path)
        request.hur_user_item[i].hui_extent.length = -1
    return request



class hsm_user_state (ctypes.Structure):
    """HSM_USER_STATE from https://github.com/tweag/lustre/blob/75f01f8bdff757a89c80aa4f0cc61608b9ff56e5/lustre/include/lustre/lustre_user.h
    """
    _fields_ = [
        ('hus_states', ctypes.c_uint32),
        ('hus_archive_id', ctypes.c_uint32),
        ('hus_in_progress_state', ctypes.c_uint32),
        ('hus_in_progress_action', ctypes.c_uint32),
        ('hus_in_progress_location_offset', ctypes.c_uint64),
        ('hus_in_progress_location_length', ctypes.c_uint64),
#        ('hus_extended_info', ctypes.c_char_p),
    ]

hsm_user_state_ptr = ctypes.POINTER(hsm_user_state)


def llapi_hsm_state_get_fd(fd):
    """
    """

    # https://github.com/tweag/lustre/blob/75f01f8bdff757a89c80aa4f0cc61608b9ff56e5/lustre/include/lustre/lustre_user.h
    # #define LL_IOC_HSM_STATE_GET_IOR('f', 211, struct hsm_user_state)
    #iint rc;
    #rc = ioctl(fd, LL_IOC_HSM_STATE_GET, hus);
    #/* If error, save errno value */
    #rc = rc ? -errno : 0;
    #return rc;

    state = hsm_user_state()
    #print fd, LL_IOC_HSM_STATE_GET, ctypes.byref (state)
    #ioctl(3, _IOC(_IOC_READ, 0x66, 0xd3, 0x20), 0x7ffe77b5b810) = 0
    LL_IOC_HSM_STATE_GET = IOR(ord('f'), 211, hsm_user_state)
    fcntl.ioctl (fd, LL_IOC_HSM_STATE_GET, hsm_user_state_ptr.from_address(ctypes.addressof (state)), True)
    return state


def llapi_hsm_state_get(path):
    with  open (path, 'rb') as f:
        fd = f.fileno()
        fcntl.fcntl(fd, fcntl.F_SETFL,  os.O_NONBLOCK)
        state = llapi_hsm_state_get_fd(fd)
        return state

def llapi_path2fid(path):
    """
    """
    #define LL_IOC_PATH2FID                 _IOR ('f', 173, long)
    with  open (path, 'rb') as f:
        fd = f.fileno()
        fcntl.fcntl(fd, fcntl.F_SETFL,  os.O_NONBLOCK, os.O_NOFOLLOW)
        LL_IOC_PATH2FID = IOR(ord('f'), 173, ctypes.c_long)
        fid = lu_fid()
        fcntl.ioctl(fs, LL_IOC_PATH2FID, lu_fid_ptr.from_address(ctypes.addressof(fid)), True)
        return fid


def get_root_path(path):
    pass

def llapi_hsm_request (path, request):
    #LL_IOC_HSM_REQUEST_IOW('f', 217, struct hsm_user_request)
    LL_IOC_HSM_REQUEST = IOW(ord('f'), 217, hsm_user_request)
    fd = get_root_path (path)

    fcntl.ioctl (fd, LL_IOC_HSM_REQUEST, hsm_request_ptr.from_adress(ctypes.addressof (request)), True)

def get_hsm_state_labels(paths):
    for path in paths:
        path, state = llapi_hsm_state_get (path)
        print ("HSM_STATE", path, state)
    return 0

def make_hsm_request (paths, action):
    """
    """
    hur = create_hsm_user_request(paths, action)
    llapi_hsm_request (hur)
    return None


def hsm_archive (paths):
    return make_hsm_request (paths, hsm_action.HUA_ARCHIVE)
    return 0

def hsm_release (paths):
    make_hsm_request (paths, hsm_action.HUA_RELEASE)
    return 0

def hsm_remove (paths):
    make_hsm_request (paths, hsm_action.HUA_REMOVE)
    return 0

def hsm_restore (paths):
    make_hsm_request (paths, hsm_action.HUA_RESTORE)
    return 0

#print LL_IOC_HSM_STATE_GET
#print llapi_hsm_state_get ('/etc/passwd')

COMMANDS = {
    'archive': hsm_archive,
    'release': hsm_release,
    'remove': hsm_remove,
    'restore': hsm_restore,
    'status': get_hsm_state_labels,
    }


def main():
    import argparse
    parser = argparse.ArgumentParser (description="LFS hsm_command paths")
    parser.add_argument ('command', choices = ["archive", "remove", "restore", "release", "status"])
    parser.add_argument ('paths', nargs='+', help="paths of files" )
    args = parser.parse_args()

    if args.command == "status":
        rc = get_hsm_state_labels(args.paths)
    elif args.command == "archive":
        rc =  hsm_archive (args.paths)

    return rc


if __name__ == "__main__":
    main()
